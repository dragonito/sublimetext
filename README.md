SublimeText
===========

My special configuration for developing php, js, html and css

SublimeText configs and extensions:

Extensions / Plugins:

- Sublime Alignment
- Bootstrap 3 Snippets
- Doctrine Snippets
- Emmet
- Javascript Beautify
- LiveStyle
- Package Control
- PHP Code Coverage
- PHP Completions Kit
- PHP Namespace Command
- PHP-Twig
- Phpcs
- PhpDoc
- PhpNamespace
- PHPUnit
- SublimeCodeIntel
- sublimelint
- SublimeLinter
- Tag
- Web Inspector 3

## Some Keystrokes

### Windows
- Comment a line or block: strg+#
- Open console strg+` on german keyboard strg+ö
- For Sublime Aligment strg+alt+a on windows

### Mac
- Delete Line: shift+alt+k
